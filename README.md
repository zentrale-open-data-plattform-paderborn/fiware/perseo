# Perseo

## General
Perseo is an [Esper-based](http://www.espertech.com/esper/) Complex Event Processing (CEP) software designed to be fully *NGSIv2*-compliant. It uses NGSIv2 as the communication protocol for events, and thus, Perseo is able to seamless and jointly work with *context brokers* such as [Orion Context Broker](https://github.com/telefonicaid/fiware-orion).

Original documentation can be found here: https://perseo.readthedocs.io/en/latest/

## Usage of Perseo
We don't use Perseo, but provide an example of how to use within the [Installation How-To's](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/master/howtos/PerseoConfig.md).

To learn more about Perseo API's you can refer to official documentation: https://perseo.readthedocs.io/en/latest/API/api/

## Versioning  
Tested on:  
Perseo-fe telefonicaiot/perseo-fe:1.13.0 (DockerHub digest: sha256:2998f554b4d13f419186d93c8f8b34a7652ee0af83bbb9362a582bd835946a20)  
Perseo-core Version telefonicaiot/perseo-core (DockerHub digest: sha256:e767576fb4c1ee3afb07c8fc0e1a89b3f975b62a638adb8f41d81db61c5397a1)

## Volume Usage
This component does not use persistent volumes.

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Perseo can be connected to via: https://perseo.fiware.opendata-CITY.de (No GUI available)

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
### Kubernetes Secrets
Create kubernetes-secret for email service credentials  
`kubectl create secret generic email-secret --from-literal=host=EMAILHOST --from-literal=user=EMAILUSER --from-literal=password=EMAILPASSWORD --namespace=fiware-dev|fiware-staging|fiware-prod`  

Create kubernetes-secret for Perseo configuration file  
```
sudo cat <<EOF >config.js
'use strict';

var config = {};

config.logLevel = 'INFO';

config.endpoint = {
    host: 'localhost',
    port: 9090,
    rulesPath: '/rules',
    actionsPath: '/actions/do',
    noticesPath: '/notices',
    vrPath: '/m2m/vrules',
    checkPath: '/check',
    versionPath: '/version',
    logPath: '/admin/log',
    metricsPath: '/admin/metrics'
};

config.isMaster = true;

config.slaveDelay = 500;

config.mongo = {
    // The URI to use for the database connection. It supports replica set URIs.
    // mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
    // I.e.: 'mongodb://user:pass@host1:27017,host2:27018,host3:27019/cep?replicaSet=myrep'
    url: 'mongodb://localhost:27017/cep'
};

config.orionDb = {
    url: 'mongodb://localhost:27017/orion',
    collection: 'entities',
    prefix: 'orion',
    batchSize: 500
};

config.perseoCore = {
    rulesURL: 'http://localhost:8080/perseo-core/rules',
    noticesURL: 'http://localhost:8080/perseo-core/events',
    interval: 60e3 * 5
};

config.nextCore = {
};

config.smtp = {
    port: 25,
    host: 'host',
    secure: false,
    auth: {
     user: 'email',
     pass: 'password'
     }
     ,
    tls: {
        rejectUnauthorized: false
    }
};

config.sms = {
    URL: 'http://sms-endpoint/smsoutbound',
    API_KEY: '',
    API_SECRET: '',
    from: 'tel:22012;phone-context=+34'
};

config.smpp = {
    host: '',
    port: '',
    systemid: '',
    password: '',
    from: '346666666',
    enabled: false
};

config.orion = {
    URL: 'http://orion-endpoint:1026'
};

config.pep = {
    URL: 'http://pep-endpoint:1026'
};

config.authentication = {
    host: 'keyrock',
    port: '3000',
    user: 'KEYROCK_ADMIN_EMAIL',
    password: 'KEYROCK_ADMIN_PASSWORD'
};

config.collections = {
    rules: 'rules',
    executions: 'executions'
};

config.executionsTTL = 1 * 24 * 60 * 60;

config.DEFAULT_SUBSERVICE = '/';
config.DEFAULT_SERVICE = 'unknownt';

config.checkDB = {
    delay: 5e3,
    reconnectTries: 1e3,
    reconnectInterval: 5e3,
    bufferMaxEntries: 5
};

config.restBase = null;

config.castTypes = false;

module.exports = config;

EOF
```  

`kubectl create secret generic perseo-config --from-file=config.js --namespace=fiware-dev|fiware-staging|fiware-prod`

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
