# Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh
# This file is covered by the EUPL license.
# You may obtain a copy of the licence at
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

FROM telefonicaiot/perseo-fe:1.13.0

COPY config/perseo /opt/perseo-fe/bin/